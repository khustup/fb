#pragma once

#include "exception.hpp"

#include <deque>
#include <list>
#include <string>
#include <vector>

namespace json {

class visitor;
class const_visitor;

template <typename ValueTypeT>
class trivial_type;

typedef trivial_type<double> number;
typedef trivial_type<bool> boolean;
typedef trivial_type<std::string> string;

class object;
class array;
class null;

class exception : public base::exception
{
public:
   exception(const std::string&);
};

/////////////////////////////////////////////////////////////////////////
// unknown_element - provides a typesafe surrogate for any of the JSON-
//  sanctioned element types. This class allows the array and object
//  class to effectively contain a heterogeneous set of child elements.
// The cast operators provide convenient implicit downcasting, while
//  preserving dynamic type safety by throwing an exception during a
//  a bad cast. 
// The object & array element index operators (operators [std::string]
//  and [size_t]) provide convenient, quick access to child elements.
//  They are a logical extension of the cast operators. These child
//  element accesses can be chained together, allowing the following
//  (when document structure is well-known):
//  string str = objInvoices[1]["Customer"]["Company"];

class unknown_element
{
public:
   unknown_element();
   unknown_element(const unknown_element&);
   unknown_element(const object&);
   unknown_element(const array&);
   unknown_element(const number&);
   unknown_element(const boolean&);
   unknown_element(const string&);
   unknown_element(const null&);

   ~unknown_element();

   unknown_element& operator = (const unknown_element& unknown);

   // implicit cast to actual element type. throws on failure
   operator const object& () const;
   operator const array& () const;
   operator const number& () const;
   operator const boolean& () const;
   operator const string& () const;
   operator const null& () const;

   // implicit cast to actual element type. *converts* on failure, and always returns success
   operator object& ();
   operator array& ();
   operator number& ();
   operator boolean& ();
   operator string& ();
   operator null& ();

   // provides quick access to children when real element type is object
   unknown_element& operator[] (const std::string& key);
   const unknown_element& operator[] (const std::string& key) const;

   // provides quick access to children when real element type is array
   unknown_element& operator[] (size_t index);
   const unknown_element& operator[] (size_t index) const;

   // implements visitor pattern
   void Accept(const_visitor& visitor) const;
   void Accept(visitor& visitor);

   // tests equality. first checks type, then value if possible
   bool operator == (const unknown_element& element) const;

private:
   class Imp;

   template <typename ElementTypeT>
   class Imp_T;

   class CastVisitor;
   class ConstCastVisitor;
   
   template <typename ElementTypeT>
   class CastVisitor_T;

   template <typename ElementTypeT>
   class ConstCastVisitor_T;

   template <typename ElementTypeT>
   const ElementTypeT& CastTo() const;

   template <typename ElementTypeT>
   ElementTypeT& ConvertTo();

   Imp* m_pImp;
};

/////////////////////////////////////////////////////////////////////////////////
// array - mimics std::deque<unknown_element>. The array contents are effectively 
//  heterogeneous thanks to the ElementUnknown class. push_back has been replaced 
//  by more generic insert functions.
class array
{
public:
   typedef std::deque<unknown_element> elements;
   typedef elements::iterator iterator;
   typedef elements::const_iterator const_iterator;

   iterator begin();
   iterator end();
   const_iterator begin() const;
   const_iterator end() const;
   
   iterator Insert(const unknown_element& element, iterator itWhere);
   iterator Insert(const unknown_element& element);
   iterator Erase(iterator itWhere);
   void Resize(size_t newSize);
   void Clear();

   size_t size() const;
   bool empty() const;

   unknown_element& operator[] (size_t index);
   const unknown_element& operator[] (size_t index) const;

   bool operator == (const array&) const;

private:
   elements m_Elements;
};

/////////////////////////////////////////////////////////////////////////////////
// object - mimics std::map<std::string, unknown_element>. The member value 
//  contents are effectively heterogeneous thanks to the unknown_element class
class object
{
public:
   struct member {
      member(const std::string& nameIn = std::string(), const unknown_element& elementIn = unknown_element());

      bool operator == (const member& member) const;

      std::string name;
      unknown_element element;
   };

   typedef std::list<member> members; // map faster, but does not preserve order
   typedef members::iterator iterator;
   typedef members::const_iterator const_iterator;

   bool operator == (const object&) const;

   iterator begin();
   iterator end();
   const_iterator begin() const;
   const_iterator end() const;

   size_t size() const;
   bool empty() const;

   iterator find(const std::string& name);
   const_iterator find(const std::string& name) const;

   iterator Insert(const member& member);
   iterator Insert(const member& member, iterator itWhere);
   iterator Erase(iterator itWhere);
   void Clear();

   unknown_element& operator [](const std::string& name);
   const unknown_element& operator [](const std::string& name) const;

private:
   class Finder;

   members m_Members;
};


/////////////////////////////////////////////////////////////////////////////////
// trivial_type - class template for encapsulates a simple data type, such as
//  a string, number, or boolean. Provides implicit const & noncost cast operators
//  for that type, allowing "DataTypeT type = trivialType;"


template <typename DataTypeT>
class trivial_type
{
public:
   trivial_type(const DataTypeT& t = DataTypeT()) :
       m_tValue(t) {}

   operator DataTypeT&()
   {
       return value();
   }

   operator const DataTypeT&() const
   {
       return value();
   }

   DataTypeT& value()
   {
       return m_tValue;
   }

   const DataTypeT& value() const
   {
       return m_tValue;
   }

   bool operator == (const trivial_type<DataTypeT>& trivial) const
   {
       return m_tValue == trivial.m_tValue;
   }

private:
   DataTypeT m_tValue;
};

/////////////////////////////////////////////////////////////////////////////////
// null - doesn't do much of anything but satisfy the JSON spec. It is the default
//  element type of unknown_element

class null
{
public:
   bool operator == (const null& trivial) const;
};

class reader
{
public:
   // this structure will be reported in one of the exceptions defined below
   struct location
   {
      location();

      unsigned int m_nLine;       // document line, zero-indexed
      unsigned int m_nLineOffset; // character offset from beginning of line, zero indexed
      unsigned int m_nDocOffset;  // character offset from entire document, zero indexed
   };

   // thrown during the first phase of reading. generally catches low-level problems such
   //  as errant characters or corrupt/incomplete documents
   class scan_exception : public exception
   {
   public:
      scan_exception(const std::string& sMessage, const reader::location& locError) :
         exception(sMessage),
         m_locError(locError) {}

      reader::location m_locError;
   };

   // thrown during the second phase of reading. generally catches higher-level problems such
   //  as missing commas or brackets
   class parse_exception : public exception
   {
   public:
      parse_exception(const std::string& sMessage, const reader::location& locTokenBegin, const reader::location& locTokenEnd) :
         exception(sMessage),
         m_locTokenBegin(locTokenBegin),
         m_locTokenEnd(locTokenEnd) {}

      reader::location m_locTokenBegin;
      reader::location m_locTokenEnd;
   };


   // if you know what the document looks like, call one of these...
   static void read(object&, std::istream& istr);
   static void read(array&, std::istream& istr);
   static void read(string&, std::istream& istr);
   static void read(number&, std::istream& istr);
   static void read(boolean&, std::istream& istr);
   static void read(null&, std::istream& istr);

   // ...otherwise, if you don't know, call this & visit it
   static void read(unknown_element& elementRoot, std::istream& istr);

private:
   struct token
   {
      enum type
      {
         TOKEN_OBJECT_BEGIN,  //    {
         TOKEN_OBJECT_END,    //    }
         TOKEN_ARRAY_BEGIN,   //    [
         TOKEN_ARRAY_END,     //    ]
         TOKEN_NEXT_ELEMENT,  //    ,
         TOKEN_MEMBER_ASSIGN, //    :
         TOKEN_STRING,        //    "xxx"
         TOKEN_NUMBER,        //    [+/-]000.000[e[+/-]000]
         TOKEN_BOOLEAN,       //    true -or- false
         TOKEN_NULL,          //    null
      };

      type nType;
      std::string sValue;

      // for malformed file debugging
      reader::location locBegin;
      reader::location locEnd;
   };

   class InputStream;
   class TokenStream;
   typedef std::vector<token> tokens;

   template <typename ElementTypeT>   
   static void _read(ElementTypeT& element, std::istream& istr);

   // scanning istream into token sequence
   void Scan(tokens& tokens, InputStream& inputStream);

   void EatWhiteSpace(InputStream& inputStream);
   std::string MatchString(InputStream& inputStream);
   std::string Matchnumber(InputStream& inputStream);
   std::string MatchExpectedString(InputStream& inputStream, const std::string& sExpected);

   // parsing token sequence into element structure
   void parse(unknown_element&, TokenStream& tokenStream);
   void parse(object&, TokenStream& tokenStream);
   void parse(array&, TokenStream& tokenStream);
   void parse(string&, TokenStream& tokenStream);
   void parse(number&, TokenStream& tokenStream);
   void parse(boolean&, TokenStream& tokenStream);
   void parse(null&, TokenStream& tokenStream);

   const std::string& MatchExpectedToken(token::type nExpected, TokenStream& tokenStream);
};

class visitor
{
public:
   virtual ~visitor() {}

   virtual void visit(array&) = 0;
   virtual void visit(object&) = 0;
   virtual void visit(number&) = 0;
   virtual void visit(string&) = 0;
   virtual void visit(boolean&) = 0;
   virtual void visit(null&) = 0;
};

class const_visitor
{
public:
   virtual ~const_visitor() {}

   virtual void visit(const array&) = 0;
   virtual void visit(const object&) = 0;
   virtual void visit(const number&) = 0;
   virtual void visit(const string&) = 0;
   virtual void visit(const boolean&) = 0;
   virtual void visit(const null&) = 0;
};

}
