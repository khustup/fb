#pragma once

#include "date.hpp"
#include "exception.hpp"
#include "http.hpp"
#include "gender.hpp"
#include "user.hpp"

#include <string>
#include <vector>

namespace fb {

class error : public base::exception
{
public:
    error(const std::string& m) 
        : base::exception(m)
    {
    }
};

class invalid_token : public base::exception
{
public:
    invalid_token(const std::string& m)
        : base::exception(std::string("Token is invalid: ") + m)
    {
    }
};

class invalid_id : public base::exception
{
public:
    invalid_id(const std::string& m)
        : base::exception(std::string("ID is invalid: ") + m)
    {
    }
};

/**
 * @class fb::engine
 * @brief Engine class for doing fb requests.
 */
class engine
{
public:
    /// @brief Constructor.
    /// @param token Access token which will be used in requests.
    /// @throw idvalid_token, error.
    engine(const std::string& token = "");

    /// @brief Copy constructor.
    engine(const engine&);

    /// @brief Assignment operator.
    engine& operator=(const engine&);

    /// @brief Destructor.
    ~engine();

public:
    /**
     * @brief Requests specified user data.
     * @param id ID of user.
     * @return user User object, which stores all the info about user -
     *         id, name, gender, location, birth date, email, friends list and liked pages.
     * @throw idvalid_id, error.
     */
    user get_user_data(const std::string& id);

    /**
     * @brief Downloads the profile picture of specified user and saves it in specified file.
     * @param id User ID.
     * @param filename Fiel, to store image.
     * @throw idvalid_id, error.
     */
    void download_photo(const std::string& id, const std::string& filename);

    /**
     * @brief Makes the post to specified users timeline.
     * @param id User ID.
     * @param message Post message.
     * @throw idvalid_id, error.
     */
    void make_post(const std::string& id, const std::wstring& post);

    void send_message(const std::string& id, const std::string& message);
    void like_page(const std::string& user_id, const std::string& page_id);

private:
    std::string m_access_token;
    net::http m_http;
};

}
