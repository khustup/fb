#include "exception.hpp"

namespace base {

exception::exception(const std::string& m)
    : m_message(m)
{
}

const std::string& exception::what() const
{
    return m_message;
}

}
