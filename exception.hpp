#pragma once

#include <string>

namespace base {

class exception
{
public:
    exception(const std::string&);

public:
    const std::string& what() const;

private:
    std::string m_message;
};

}
