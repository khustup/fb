#include "json.hpp"

#include <cassert>
#include <algorithm>
#include <map>
#include <set>
#include <sstream>

namespace json {


exception::exception(const std::string& sMessage) :
   base::exception(sMessage) {}

class unknown_element::Imp
{
public:
   virtual ~Imp() {}
   virtual Imp* Clone() const = 0;

   virtual bool Compare(const Imp& imp) const = 0;

   virtual void Accept(const_visitor& v) const = 0;
   virtual void Accept(visitor& v) = 0;
};


template <typename ElementTypeT>
class unknown_element::Imp_T : public unknown_element::Imp
{
public:
   Imp_T(const ElementTypeT& element) : m_Element(element) {}
   virtual Imp* Clone() const { return new Imp_T<ElementTypeT>(*this); }

   virtual void Accept(const_visitor& v) const { v.visit(m_Element); }
   virtual void Accept(visitor& v) { v.visit(m_Element); }

   virtual bool Compare(const Imp& imp) const
   {
      ConstCastVisitor_T<ElementTypeT> castVisitor;
      imp.Accept(castVisitor);
      return castVisitor.m_pElement &&
             m_Element == *castVisitor.m_pElement;
   }

private:
   ElementTypeT m_Element;
};

class unknown_element::ConstCastVisitor : public const_visitor
{
   virtual void visit(const array&) {}
   virtual void visit(const object&) {}
   virtual void visit(const number&) {}
   virtual void visit(const string&) {}
   virtual void visit(const boolean&) {}
   virtual void visit(const null&) {}
};

template <typename ElementTypeT>
class unknown_element::ConstCastVisitor_T : public ConstCastVisitor
{
public:
   ConstCastVisitor_T() : m_pElement(0) {}
   virtual void visit(const ElementTypeT& element) { m_pElement = &element; } // we don't know what this is, but it overrides one of the base's no-op functions
   const ElementTypeT* m_pElement;
};

class unknown_element::CastVisitor : public visitor
{
   virtual void visit(array&) {}
   virtual void visit(object&) {}
   virtual void visit(number&) {}
   virtual void visit(string&) {}
   virtual void visit(boolean&) {}
   virtual void visit(null&) {}
};

template <typename ElementTypeT>
class unknown_element::CastVisitor_T : public CastVisitor
{
public:
   CastVisitor_T() : m_pElement(0) {}
   virtual void visit(ElementTypeT& element) { m_pElement = &element; } // we don't know what this is, but it overrides one of the base's no-op functions
   ElementTypeT* m_pElement;
};

unknown_element::unknown_element() :                               m_pImp( new Imp_T<null>( null() ) ) {}
unknown_element::unknown_element(const unknown_element& unknown) :  m_pImp( unknown.m_pImp->Clone()) {}
unknown_element::unknown_element(const object& o) :           m_pImp( new Imp_T<object>(o) ) {}
unknown_element::unknown_element(const array& a) :             m_pImp( new Imp_T<array>(a) ) {}
unknown_element::unknown_element(const number& n) :           m_pImp( new Imp_T<number>(n) ) {}
unknown_element::unknown_element(const boolean& b) :         m_pImp( new Imp_T<boolean>(b) ) {}
unknown_element::unknown_element(const string& s) :           m_pImp( new Imp_T<string>(s) ) {}
unknown_element::unknown_element(const null& n) :               m_pImp( new Imp_T<null>(n) ) {}

unknown_element::~unknown_element()   { delete m_pImp; }

unknown_element::operator const object& () const    { return CastTo<object>(); }
unknown_element::operator const array& () const     { return CastTo<array>(); }
unknown_element::operator const number& () const    { return CastTo<number>(); }
unknown_element::operator const boolean& () const   { return CastTo<boolean>(); }
unknown_element::operator const string& () const    { return CastTo<string>(); }
unknown_element::operator const null& () const      { return CastTo<null>(); }

unknown_element::operator object& ()    { return ConvertTo<object>(); }
unknown_element::operator array& ()     { return ConvertTo<array>(); }
unknown_element::operator number& ()    { return ConvertTo<number>(); }
unknown_element::operator boolean& ()   { return ConvertTo<boolean>(); }
unknown_element::operator string& ()    { return ConvertTo<string>(); }
unknown_element::operator null& ()      { return ConvertTo<null>(); }

unknown_element& unknown_element::operator = (const unknown_element& unknown) 
{
   // always check for this
   if (&unknown != this)
   {
      // we might be copying from a subtree of ourselves. delete the old imp
      //  only after the clone operation is complete. yes, this could be made 
      //  more efficient, but isn't worth the complexity
      Imp* pOldImp = m_pImp;
      m_pImp = unknown.m_pImp->Clone();
      delete pOldImp;
   }

   return *this;
}

unknown_element& unknown_element::operator[] (const std::string& key)
{
   // the people want an o. make us one if we aren't already
   object& o = ConvertTo<object>();
   return o[key];
}

const unknown_element& unknown_element::operator[] (const std::string& key) const
{
   // throws if we aren't an o
   const object& o = CastTo<object>();
   return o[key];
}

unknown_element& unknown_element::operator[] (size_t index)
{
   // the people want an a. make us one if we aren't already
   array& a = ConvertTo<array>();
   return a[index];
}

const unknown_element& unknown_element::operator[] (size_t index) const
{
   // throws if we aren't an a
   const array& a = CastTo<array>();
   return a[index];
}


template <typename ElementTypeT>
const ElementTypeT& unknown_element::CastTo() const
{
   ConstCastVisitor_T<ElementTypeT> castVisitor;
   m_pImp->Accept(castVisitor);
   if (castVisitor.m_pElement == 0)
      throw exception("Bad cast");
   return *castVisitor.m_pElement;
}

template <typename ElementTypeT>
ElementTypeT& unknown_element::ConvertTo() 
{
   CastVisitor_T<ElementTypeT> castVisitor;
   m_pImp->Accept(castVisitor);
   if (castVisitor.m_pElement == 0)
   {
      // we're not the right type. fix it & try again
      *this = ElementTypeT();
      m_pImp->Accept(castVisitor);
   }

   return *castVisitor.m_pElement;
}


void unknown_element::Accept(const_visitor& v) const { m_pImp->Accept(v); }
void unknown_element::Accept(visitor& v)            { m_pImp->Accept(v); }


bool unknown_element::operator == (const unknown_element& element) const
{
   return m_pImp->Compare(*element.m_pImp);
}

object::member::member(const std::string& nameIn, const unknown_element& elementIn) :
   name(nameIn), element(elementIn) {}

bool object::member::operator == (const member& m) const 
{
   return name == m.name &&
          element == m.element;
}

class object::Finder : public std::unary_function<object::member, bool>
{
public:
   Finder(const std::string& name) : m_name(name) {}
   bool operator () (const object::member& m) {
      return m.name == m_name;
   }

private:
   std::string m_name;
};

object::iterator object::begin() { return m_Members.begin(); }
object::iterator object::end() { return m_Members.end(); }
object::const_iterator object::begin() const { return m_Members.begin(); }
object::const_iterator object::end() const { return m_Members.end(); }

size_t object::size() const { return m_Members.size(); }
bool object::empty() const { return m_Members.empty(); }

object::iterator object::find(const std::string& name) 
{
   return std::find_if(m_Members.begin(), m_Members.end(), Finder(name));
}

object::const_iterator object::find(const std::string& name) const 
{
   return std::find_if(m_Members.begin(), m_Members.end(), Finder(name));
}

object::iterator object::Insert(const member& m)
{
   return Insert(m, end());
}

object::iterator object::Insert(const member& m, iterator itWhere)
{
   iterator it = find(m.name);
   if (it != m_Members.end())
      throw exception(std::string("object member already exists: ") + m.name);

   it = m_Members.insert(itWhere, m);
   return it;
}

object::iterator object::Erase(iterator itWhere) 
{
   return m_Members.erase(itWhere);
}

unknown_element& object::operator [](const std::string& name)
{

   iterator it = find(name);
   if (it == m_Members.end())
   {
      member m(name);
      it = Insert(m, end());
   }
   return it->element;      
}

const unknown_element& object::operator [](const std::string& name) const 
{
   const_iterator it = find(name);
   if (it == end())
      throw exception(std::string("object member not found: ") + name);
   return it->element;
}

void object::Clear() 
{
   m_Members.clear(); 
}

bool object::operator == (const object& o) const 
{
   return m_Members == o.m_Members;
}

array::iterator array::begin()  { return m_Elements.begin(); }
array::iterator array::end()    { return m_Elements.end(); }
array::const_iterator array::begin() const  { return m_Elements.begin(); }
array::const_iterator array::end() const    { return m_Elements.end(); }

array::iterator array::Insert(const unknown_element& element, iterator itWhere)
{ 
   return m_Elements.insert(itWhere, element);
}

array::iterator array::Insert(const unknown_element& element)
{
   return Insert(element, end());
}

array::iterator array::Erase(iterator itWhere)
{ 
   return m_Elements.erase(itWhere);
}

void array::Resize(size_t newSize)
{
   m_Elements.resize(newSize);
}

size_t array::size() const  { return m_Elements.size(); }
bool array::empty() const   { return m_Elements.empty(); }

unknown_element& array::operator[] (size_t index)
{
   size_t nMinSize = index + 1; // zero indexed
   if (m_Elements.size() < nMinSize)
      m_Elements.resize(nMinSize);
   return m_Elements[index]; 
}

const unknown_element& array::operator[] (size_t index) const 
{
   if (index >= m_Elements.size())
      throw exception("array out of bounds");
   return m_Elements[index]; 
}

void array::Clear() {
   m_Elements.clear();
}

bool array::operator == (const array& a) const
{
   return m_Elements == a.m_Elements;
}

bool null::operator == (const null&) const
{
   return true;
}

reader::location::location() :
   m_nLine(0),
   m_nLineOffset(0),
   m_nDocOffset(0)
{}


//////////////////////
// reader::InputStream

class reader::InputStream // would be cool if we could inherit from std::istream & override "get"
{
public:
   InputStream(std::istream& iStr) :
      m_iStr(iStr) {}

   // protect access to the input stream, so we can keeep track of document/line offsets
   char Get(); // big, define outside
   char Peek() {
      assert(m_iStr.eof() == false); // enforce reading of only valid stream data 
      return m_iStr.peek();
   }

   bool EOS() {
      m_iStr.peek(); // apparently eof flag isn't set until a character read is attempted. whatever.
      return m_iStr.eof();
   }

   const location& GetLocation() const { return m_Location; }

private:
   std::istream& m_iStr;
   location m_Location;
};


char reader::InputStream::Get()
{
   assert(m_iStr.eof() == false); // enforce reading of only valid stream data 
   char c = m_iStr.get();
   
   ++m_Location.m_nDocOffset;
   if (c == '\n') {
      ++m_Location.m_nLine;
      m_Location.m_nLineOffset = 0;
   }
   else {
      ++m_Location.m_nLineOffset;
   }

   return c;
}



//////////////////////
// reader::TokenStream

class reader::TokenStream
{
public:
   TokenStream(const tokens& ts);

   const token& Peek();
   const token& Get();

   bool EOS() const;

private:
   const tokens& m_Tokens;
   tokens::const_iterator m_itCurrent;
};


reader::TokenStream::TokenStream(const tokens& ts) :
   m_Tokens(ts),
   m_itCurrent(ts.begin())
{}

const reader::token& reader::TokenStream::Peek() {
   if (EOS())
   {
      const token& lastToken = *m_Tokens.rbegin();
      std::string sMessage = "Unexpected end of token stream";
      throw parse_exception(sMessage, lastToken.locBegin, lastToken.locEnd); // nowhere to point to
   }
   return *(m_itCurrent); 
}

const reader::token& reader::TokenStream::Get() {
   const token& t = Peek();
   ++m_itCurrent;
   return t;
}

bool reader::TokenStream::EOS() const {
   return m_itCurrent == m_Tokens.end(); 
}

void reader::read(object& o, std::istream& istr) { _read(o, istr); }
void reader::read(array& a, std::istream& istr) { _read(a, istr); }
void reader::read(string& s, std::istream& istr) { _read(s, istr); }
void reader::read(number& n, std::istream& istr) { _read(n, istr); }
void reader::read(boolean& b, std::istream& istr) { _read(b, istr); }
void reader::read(null& n, std::istream& istr) { _read(n, istr); }
void reader::read(unknown_element& unknown, std::istream& istr) { _read(unknown, istr); }


template <typename ElementTypeT>   
void reader::_read(ElementTypeT& element, std::istream& istr)
{
   reader reader;

   tokens ts;
   InputStream inputStream(istr);
   reader.Scan(ts, inputStream);

   TokenStream tokenStream(ts);
   reader.parse(element, tokenStream);

   if (tokenStream.EOS() == false)
   {
      const token& t = tokenStream.Peek();
      std::string sMessage = std::string("Expected end of token stream; found ") + t.sValue;
      throw parse_exception(sMessage, t.locBegin, t.locEnd);
   }
}


void reader::Scan(tokens& ts, InputStream& inputStream)
{
   while (EatWhiteSpace(inputStream),              // ignore any leading white space...
          inputStream.EOS() == false) // ...before checking for EOS
   {
      // if all goes well, we'll create a token each pass
      token t;
      t.locBegin = inputStream.GetLocation();

      // gives us null-terminated string
      char sChar = inputStream.Peek();
      switch (sChar)
      {
         case '{':
            t.sValue = MatchExpectedString(inputStream, "{");
            t.nType = token::TOKEN_OBJECT_BEGIN;
            break;

         case '}':
            t.sValue = MatchExpectedString(inputStream, "}");
            t.nType = token::TOKEN_OBJECT_END;
            break;

         case '[':
            t.sValue = MatchExpectedString(inputStream, "[");
            t.nType = token::TOKEN_ARRAY_BEGIN;
            break;

         case ']':
            t.sValue = MatchExpectedString(inputStream, "]");
            t.nType = token::TOKEN_ARRAY_END;
            break;

         case ',':
            t.sValue = MatchExpectedString(inputStream, ",");
            t.nType = token::TOKEN_NEXT_ELEMENT;
            break;

         case ':':
            t.sValue = MatchExpectedString(inputStream, ":");
            t.nType = token::TOKEN_MEMBER_ASSIGN;
            break;

         case '"':
            t.sValue = MatchString(inputStream);
            t.nType = token::TOKEN_STRING;
            break;

         case '-':
         case '0':
         case '1':
         case '2':
         case '3':
         case '4':
         case '5':
         case '6':
         case '7':
         case '8':
         case '9':
            t.sValue = Matchnumber(inputStream);
            t.nType = token::TOKEN_NUMBER;
            break;

         case 't':
            t.sValue = MatchExpectedString(inputStream, "true");
            t.nType = token::TOKEN_BOOLEAN;
            break;

         case 'f':
            t.sValue = MatchExpectedString(inputStream, "false");
            t.nType = token::TOKEN_BOOLEAN;
            break;

         case 'n':
            t.sValue = MatchExpectedString(inputStream, "null");
            t.nType = token::TOKEN_NULL;
            break;

         default:
         {
            std::string sErrorMessage = std::string("Unexpected character in stream: ") + sChar;
            throw scan_exception(sErrorMessage, inputStream.GetLocation());
         }
      }

      t.locEnd = inputStream.GetLocation();
      ts.push_back(t);
   }
}


void reader::EatWhiteSpace(InputStream& inputStream)
{
   while (inputStream.EOS() == false && 
          ::isspace(inputStream.Peek()))
      inputStream.Get();
}

std::string reader::MatchExpectedString(InputStream& inputStream, const std::string& sExpected)
{
   std::string::const_iterator it(sExpected.begin()),
                               itEnd(sExpected.end());
   for ( ; it != itEnd; ++it) {
      if (inputStream.EOS() ||      // did we reach the end before finding what we're looking for...
          inputStream.Get() != *it) // ...or did we find something different?
      {
         std::string sMessage = std::string("Expected string: ") + sExpected;
         throw scan_exception(sMessage, inputStream.GetLocation());
      }
   }

   // all's well if we made it here
   return sExpected;
}


std::string reader::MatchString(InputStream& inputStream)
{
   MatchExpectedString(inputStream, "\"");

   std::string s;
   while (inputStream.EOS() == false &&
          inputStream.Peek() != '"')
   {
      char c = inputStream.Get();

      // escape?
      if (c == '\\' &&
          inputStream.EOS() == false) // shouldn't have reached the end yet
      {
         c = inputStream.Get();
         switch (c) {
            case '/':      s.push_back('/');     break;
            case '"':      s.push_back('"');     break;
            case '\\':     s.push_back('\\');    break;
            case 'b':      s.push_back('\b');    break;
            case 'f':      s.push_back('\f');    break;
            case 'n':      s.push_back('\n');    break;
            case 'r':      s.push_back('\r');    break;
            case 't':      s.push_back('\t');    break;
            case 'u':      s.push_back('\\'); s.push_back('u');    break; // TODO: what do we do with this?
            default: {
               std::string sMessage = std::string("Unrecognized escape sequence found in string: \\") + c;
               throw scan_exception(sMessage, inputStream.GetLocation());
            }
         }
      }
      else {
         s.push_back(c);
      }
   }

   // eat the last '"' that we just peeked
   MatchExpectedString(inputStream, "\"");

   // all's well if we made it here
   return s;
}


std::string reader::Matchnumber(InputStream& inputStream)
{
   const char sNumericChars[] = "0123456789.eE-+";
   std::set<char> numericChars;
   numericChars.insert(sNumericChars, sNumericChars + sizeof(sNumericChars));

   std::string snumber;
   while (inputStream.EOS() == false &&
          numericChars.find(inputStream.Peek()) != numericChars.end())
   {
      snumber.push_back(inputStream.Get());   
   }

   return snumber;
}


void reader::parse(unknown_element& element, reader::TokenStream& tokenStream) 
{
   const token& t = tokenStream.Peek();
   switch (t.nType) {
      case token::TOKEN_OBJECT_BEGIN:
      {
         // implicit non-const cast will perform conversion for us (if necessary)
         object& o = element;
         parse(o, tokenStream);
         break;
      }

      case token::TOKEN_ARRAY_BEGIN:
      {
         array& a = element;
         parse(a, tokenStream);
         break;
      }

      case token::TOKEN_STRING:
      {
         string& s = element;
         parse(s, tokenStream);
         break;
      }

      case token::TOKEN_NUMBER:
      {
         number& n = element;
         parse(n, tokenStream);
         break;
      }

      case token::TOKEN_BOOLEAN:
      {
         boolean& b = element;
         parse(b, tokenStream);
         break;
      }

      case token::TOKEN_NULL:
      {
         null& n= element;
         parse(n, tokenStream);
         break;
      }

      default:
      {
         std::string sMessage = std::string("Unexpected token: ") + t.sValue;
         throw parse_exception(sMessage, t.locBegin, t.locEnd);
      }
   }
}


void reader::parse(object& o, reader::TokenStream& tokenStream)
{
   MatchExpectedToken(token::TOKEN_OBJECT_BEGIN, tokenStream);

   bool bContinue = (tokenStream.EOS() == false &&
                     tokenStream.Peek().nType != token::TOKEN_OBJECT_END);
   while (bContinue)
   {
      object::member m;

      // first the member name. save the token in case we have to throw an exception
      const token& tokenName = tokenStream.Peek();
      m.name = MatchExpectedToken(token::TOKEN_STRING, tokenStream);

      // ...then the key/value separator...
      MatchExpectedToken(token::TOKEN_MEMBER_ASSIGN, tokenStream);

      // ...then the value itself (can be anything).
      parse(m.element, tokenStream);

      // try adding it to the o (this could throw)
      try
      {
         o.Insert(m);
      }
      catch (exception&)
      {
         // must be a duplicate name
         std::string sMessage = std::string("Duplicate o member token: ") + m.name; 
         throw parse_exception(sMessage, tokenName.locBegin, tokenName.locEnd);
      }

      bContinue = (tokenStream.EOS() == false &&
                   tokenStream.Peek().nType == token::TOKEN_NEXT_ELEMENT);
      if (bContinue)
         MatchExpectedToken(token::TOKEN_NEXT_ELEMENT, tokenStream);
   }

   MatchExpectedToken(token::TOKEN_OBJECT_END, tokenStream);
}


void reader::parse(array& a, reader::TokenStream& tokenStream)
{
   MatchExpectedToken(token::TOKEN_ARRAY_BEGIN, tokenStream);

   bool bContinue = (tokenStream.EOS() == false &&
                     tokenStream.Peek().nType != token::TOKEN_ARRAY_END);
   while (bContinue)
   {
      // ...what's next? could be anything
      array::iterator itElement = a.Insert(unknown_element());
      unknown_element& element = *itElement;
      parse(element, tokenStream);

      bContinue = (tokenStream.EOS() == false &&
                   tokenStream.Peek().nType == token::TOKEN_NEXT_ELEMENT);
      if (bContinue)
         MatchExpectedToken(token::TOKEN_NEXT_ELEMENT, tokenStream);
   }

   MatchExpectedToken(token::TOKEN_ARRAY_END, tokenStream);
}


void reader::parse(string& s, reader::TokenStream& tokenStream)
{
   s = MatchExpectedToken(token::TOKEN_STRING, tokenStream);
}


void reader::parse(number& n, reader::TokenStream& tokenStream)
{
   const token& currentToken = tokenStream.Peek(); // might need this later for throwing exception
   const std::string& sValue = MatchExpectedToken(token::TOKEN_NUMBER, tokenStream);

   std::istringstream iStr(sValue);
   double dValue;
   iStr >> dValue;

   // did we consume all characters in the token?
   if (iStr.eof() == false)
   {
      char c = iStr.peek();
      std::string sMessage = std::string("Unexpected character in NUMBER token: ") + c;
      throw parse_exception(sMessage, currentToken.locBegin, currentToken.locEnd);
   }

   n = dValue;
}


void reader::parse(boolean& b, reader::TokenStream& tokenStream)
{
   const std::string& sValue = MatchExpectedToken(token::TOKEN_BOOLEAN, tokenStream);
   b = (sValue == "true" ? true : false);
}


void reader::parse(null&, reader::TokenStream& tokenStream)
{
   MatchExpectedToken(token::TOKEN_NULL, tokenStream);
}


const std::string& reader::MatchExpectedToken(token::type nExpected, reader::TokenStream& tokenStream)
{
   const token& t = tokenStream.Get();
   if (t.nType != nExpected)
   {
      std::string sMessage = std::string("Unexpected token: ") + t.sValue;
      throw parse_exception(sMessage, t.locBegin, t.locEnd);
   }

   return t.sValue;
}

}
