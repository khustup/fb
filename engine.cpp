#include "engine.hpp"
#include "json.hpp"
#include "utility.hpp"

#include <cstdlib>
#include <fstream>
#include <sstream>


#include <iostream>

namespace fb {

namespace {

const std::string s_graph_url = "https://graph.facebook.com/"; 
void check_token(const std::string& token, net::http& h)
{
    std::string url = s_graph_url + "?access_token=" + token;
    std::stringstream ss(h.get(url));
    json::object jo;
    json::reader::read(jo, ss);
    if (jo.size() != 1) {
        throw error("Unexpected responce from facebook.");
    }
    json::object::member& m = *(jo.begin());
    if (m.name != "error") {
        throw error("Unexpected responce from facebook.");
    }
    json::object& ee = m.element;
    json::object::const_iterator i = ee.find("message");
    if (i == ee.end()) {
        throw error("Unexpected responce from facebook.");
    }
    std::string message = static_cast<json::string>(i->element).value();
    if (message != std::string("Unsupported get request.")) {
        throw invalid_token(message);
    }
}

}

engine::engine(const std::string& token)
    : m_access_token(token)
    , m_http()
{
    check_token(m_access_token, m_http);
}

engine::~engine()
{
}

namespace {

void handle_error(const json::object& o)
{
    json::object::const_iterator i = o.find("error");
    if (i != o.end()) {
        json::object e = i->element;
        json::object::const_iterator i = e.find("message");
        if (i == e.end()) {
            throw error("Unexpected responce from facebook.");
        }
        std::string message = static_cast<json::string>(i->element).value();
        throw invalid_id(message);
    }
}

json::object get_json_object(const std::string& id, const std::string& fields, const std::string& token, net::http& h, const std::string& other = "")
{
    std::string url = s_graph_url + id + "?fields=" + fields + "&access_token=" + token;
    if (!other.empty()) {
        url += "&" + other;
    }
    std::stringstream ss(h.get(url));
    json::object jo;
    json::reader::read(jo, ss);
    return jo;
}

void set_full_name(const json::object& jo, user& u)
{
    json::object::const_iterator i = jo.find("first_name");
    if (i != jo.end()) {
        u.first_name = base::json_to_unicode(static_cast<json::string>(i->element).value());
    }
    i = jo.find("last_name");
    if (i != jo.end()) {
        u.last_name = base::json_to_unicode(static_cast<json::string>(i->element).value());
    }
}

void set_gender(const json::object& jo, user& u)
{
    json::object::const_iterator i = jo.find("gender");
    if (i == jo.end()) {
        return;
    }
    const std::string& gender = static_cast<json::string>(i->element).value();
    if (gender == "male") {
        u.gender = base::MALE;
    } else if (gender == "female") {
        u.gender = base::FEMALE;
    } else {
        throw error("Unexpected responce from facebook.(invalid gender)");
    }
}

void set_birth_date(const json::object& jo, user& u)
{
    json::object::const_iterator i = jo.find("birthday");
    if (i == jo.end()) {
        return;
    }
    const std::string& birthday = static_cast<json::string>(i->element).value();
    if (birthday.size() != 10 || birthday[2] != '/' || birthday[5] != '/') {
        throw error("Unexpected responce from facebook.(invalid date)");
    }
    std::string m = birthday.substr(0, 2);
    try {
        u.birth_date.set_month(std::atoi(m.c_str()));
    } catch (base::invalid_date&) {
        throw error("Unexpected responce from facebook.(invalid date)");
    }
    m = birthday.substr(3, 2);
    try {
        u.birth_date.set_day(std::atoi(m.c_str()));
    } catch (base::invalid_date&) {
        throw error("Unexpected responce from facebook.(invalid date)");
    }
    m = birthday.substr(6);
    try {
        u.birth_date.set_year(std::atoi(m.c_str()));
    } catch (base::invalid_date&) {
        throw error("Unexpected responce from facebook.(invalid date)");
    }
}

void set_email(const json::object& jo, user& u)
{
    json::object::const_iterator i = jo.find("email");
    if (i == jo.end()) {
        return;
    }
    std::string& email = static_cast<json::string>(i->element).value();
    std::string::size_type x = email.find("\\u0040");
    if (x != std::string::npos) {
        email.replace(x, 6, "@");
    }
    u.email = email;
}

void set_location(const json::object& jo, user& u)
{
    json::object::const_iterator i = jo.find("location");
    if (i == jo.end()) {
        return;
    }
    const json::object& ll = i->element;
    i = ll.find("name");
    if (i == jo.end()) {
        return;
    }
    u.location = base::json_to_unicode(static_cast<json::string>(i->element).value());
}

void get_friends(const std::string& id, const std::string& token, net::http& h, user& u)
{
    json::object jo = get_json_object(id + "/friends", "", token, h, "limit=5000");
    if (jo.find("error") != jo.end()) {
        return;
    }
    json::object::const_iterator i = jo.find("data");
    if (i == jo.end()) {
        return;
    }
    const json::array& fs = i->element;
    std::vector<std::string> v;
    for (json::array::const_iterator x = fs.begin(); x != fs.end(); ++x) {
        const json::object& f = *x;
        json::object::const_iterator j = f.find("id");
        if (j == f.end()) {
            continue;
        }
        u.friends.push_back(static_cast<json::string>(j->element).value());
    }
}

void get_liked_pages(const std::string& id, const std::string& token, net::http& h, user& u)
{
    json::object jo = get_json_object(id + "/likes", "", token, h, "limit=1000000");
    if (jo.find("error") != jo.end()) {
        return;
    }
    json::object::const_iterator i = jo.find("data");
    if (i == jo.end()) {
        return;
    }
    const json::array& fs = i->element;
    std::vector<std::string> v;
    for (json::array::const_iterator x = fs.begin(); x != fs.end(); ++x) {
        const json::object& f = *x;
        json::object::const_iterator j = f.find("id");
        if (j == f.end()) {
            continue;
        }
        u.liked_pages.push_back(static_cast<json::string>(j->element).value());
    }
}

}

user engine::get_user_data(const std::string& id)
{
    json::object jo = get_json_object(id, "first_name,last_name,location,gender,birthday,email", m_access_token, m_http);
    handle_error(jo);
    user u;
    u.id = id;
    set_full_name(jo, u);
    set_gender(jo, u);
    set_birth_date(jo, u);
    set_email(jo, u);
    set_location(jo, u);
    get_friends(id, m_access_token, m_http, u);
    get_liked_pages(id, m_access_token, m_http, u);
    return u;
}

void engine::download_photo(const std::string& id, const std::string& filename)
{
    std::string url = s_graph_url + id + "/picture?type=large&access_token=" + m_access_token;
    std::string content = m_http.get(url);
    std::ofstream f(filename.c_str(), std::ios_base::binary | std::ios_base::out);
    for (unsigned i = 0; i < content.size(); ++i) {
        f.put(content[i]);
    }
    f.close();
}

void engine::make_post(const std::string& id, const std::wstring& post)
{
    net::http::params p;
    p["access_token"] = m_access_token;
    p["message"] = base::to_ascii(post);
    std::stringstream ss(m_http.post(s_graph_url + id + "/feed", p));
    json::object jo;
    json::reader::read(jo, ss);
    handle_error(jo);
}

}
