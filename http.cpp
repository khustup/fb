#include "http.hpp"

namespace net {

error::error(const std::string& m)
    : exception(m)
{
}

namespace {

std::string s_data;

size_t data_get(void* ptr, size_t size, size_t nmemb, void*)
{
    s_data += std::string(static_cast<char*>(ptr), static_cast<char*>(ptr) + nmemb);
    return size * nmemb;
}

}

http::http()
    : m_curl(curl_easy_init())
{
    if (m_curl == NULL) {
        throw error(std::string("Failed to initialize CURL"));
    }
    curl_easy_setopt(m_curl, CURLOPT_FOLLOWLOCATION, 1L);
    curl_easy_setopt(m_curl, CURLOPT_WRITEFUNCTION, &data_get);
    curl_easy_setopt(m_curl, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:24.0) Gecko/20100101 Firefox/24.0");
}

http::~http()
{
    curl_easy_cleanup(m_curl);
}

std::string http::get(const std::string& url)
{
    CURLcode res;
    curl_easy_setopt(m_curl, CURLOPT_HTTPGET, 1);
    curl_easy_setopt(m_curl, CURLOPT_URL, url.c_str());
    res = curl_easy_perform(m_curl);
    if(res != CURLE_OK) {
        throw error(std::string(curl_easy_strerror(res)));
    }
    std::string r = s_data;
    s_data.clear();
    return r;
}

std::string http::post(const std::string& url, const params& p)
{
    CURLcode res;
    curl_easy_setopt(m_curl, CURLOPT_URL, url.c_str());
    struct curl_httppost* formpost=NULL;
    struct curl_httppost* lastptr=NULL;
    params::const_iterator i = p.begin();
    while (i != p.end()) {
        curl_formadd(&formpost,
                &lastptr,
                CURLFORM_COPYNAME, i->first.c_str(),
                CURLFORM_COPYCONTENTS, i->second.c_str(),
                CURLFORM_END);
        ++i;
    }
    curl_easy_setopt(m_curl, CURLOPT_HTTPPOST, formpost);
    res = curl_easy_perform(m_curl);
    curl_formfree(formpost);
    if(res != CURLE_OK) {
        throw error(std::string(curl_easy_strerror(res)));
    }
    std::string r = s_data;
    s_data.clear();
    return r;
}

}
