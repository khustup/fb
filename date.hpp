#pragma once

#include "exception.hpp"

namespace base {

class invalid_date : public base::exception
{
public:
    invalid_date(const std::string& m)
        : base::exception(m)
    {
    }
};

class date
{
public:
    /// @throw invalid_date
    date(unsigned y = 0, unsigned m = 1, unsigned d = 1)
        : m_year(y)
        , m_month(m)
        , m_day(d)
    {
        check_date(y, m, d);
    }

    unsigned get_year() const
    {
        return m_year;
    }

    unsigned get_month() const
    {
        return m_month;
    }

    unsigned get_day() const
    {
        return m_day;
    }

    /// @throw invalid_date
    void set_year(unsigned y)
    {
        check_date(y, m_month, m_day);
        m_year = y;
    }

    /// @throw invalid_date
    void set_month(unsigned m)
    {
        check_date(m_year, m, m_day);
        m_month = m;
    }

    /// @throw invalid_date
    void set_day(unsigned d)
    {
        check_date(m_year, m_month, d);
        m_day = d;
    }

private:
    void check_date(unsigned y, unsigned m, unsigned d);

private:
    unsigned m_year;
    unsigned m_month;
    unsigned m_day;
};

}
