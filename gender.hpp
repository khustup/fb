#pragma once

namespace base {

enum gender {
    MALE,
    FEMALE,
    UNKNOWN
};

}
