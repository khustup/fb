#include "engine.hpp"

#include <iostream>

int main(int argc, char** argv)
{
    if (argc != 2) {
        std::cerr << "Invalid command line arguments. Should be \"app_name\" \"access_token\"" << std::endl;
        return 1;
    }
    try {
        fb::engine e(argv[1]);
        fb::user u = e.get_user_data("me");
        std::cout << "Gender: ";
        if (u.gender != base::UNKNOWN) {
            if (u.gender == base::FEMALE) {
                std::cout << "female";
            } else {
                std::cout << "male";
            }
        } else {
            std::cout << "unknown";
        }
        std::cout << std::endl;
        std::cout << "Birth Date: " << u.birth_date.get_month() << "/" << u.birth_date.get_day() << "/" << u.birth_date.get_year() << std::endl;
        std::cout << "Email: " << u.email << std::endl;
        std::cout << "Friends: ";
        for (unsigned i = 0; i < u.friends.size(); ++i) {
            std::cout << u.friends[i] << " ";
        }
        std::cout << std::endl;
        std::cout << "Liked pages: ";
        for (unsigned i = 0; i < u.liked_pages.size(); ++i) {
            std::cout << u.liked_pages[i] << " ";
        }
        std::cout << std::endl;
        std::wcout << "Location: " << u.location << std::endl;
        std::wcout << "First Name: " << u.first_name << std::endl;
        std::wcout << "Last Name: " << u.last_name << std::endl;
        e.download_photo("me", "pic.jpg");
        e.make_post("me", L"Հայերեն պոստ ԱՊԻ֊ից։ Բա բա բա");
    } catch (base::exception& e) {
        std::cout << e.what() << std::endl;
    }
    return 0;
}
