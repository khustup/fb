#pragma once

#include <string>

namespace base {

std::wstring json_to_unicode(const std::string& s);

std::wstring to_unicode(const std::string& s);

std::string to_ascii(const std::wstring& s);
}
