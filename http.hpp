#pragma once

#include "exception.hpp"

#include <curl/curl.h>

#include <map>
#include <string>

namespace net {

class error : public base::exception
{
public:
    error(const std::string&);
};

/// @class http.
/// @brief Class for sending http requests.
class http
{
public:
    /// @brief Exception class for http request errors.
    /**
     * @brief Sends GET request to specified url.
     * @param url URL to request.
     * @return The response body.
     * @throw error
     * @note This function is not thread safe.
     */
    std::string get(const std::string& url);

    /// @brief Parameters map, for POST request.
    typedef std::map<std::string, std::string> params;

    /**
     * @brief Sends POST request to specified url.
     * @param url URL to request.
     * @param p Parameters map.
     * @return The response body.
     * @throw error
     * @note This function is not thread safe.
     */
    std::string post(const std::string& url, const params& p);

public:
    http();
    ~http();

private:
    http(const http&);
    http& operator= (const http&);

private:
    CURL* m_curl;
};

}
