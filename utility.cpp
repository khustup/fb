#include "utility.hpp"

#include <boost/locale.hpp>

namespace base {

namespace {

char char_to_value(unsigned char c)
{
    if (c >= '0' && c <= '9') {
        return c - '0';
    }
    if (c >= 'a' && c <= 'f') {
        return 10 + (c - 'a');
    }
    if (c >= 'A' && c <= 'F') {
        return 10 + (c - 'A');
    }
    return -1;
}

}

std::wstring json_to_unicode(const std::string& s)
{
    std::wstring w;
    std::string::size_type i = 0;
    while (i < s.size()) {
        if (s[i] == '\\' && s[i + 1] == 'u' && i + 5 < s.size()) {
            wchar_t t = 0;
            bool success = true;
            for (int j = 0; j < 4; ++j) {
                unsigned char cc = s[i + j + 2];
                char ccc = char_to_value(cc);
                if (ccc != -1) {
                    t = t << 4;
                    t |= ccc;
                } else {
                    ++i;
                    success = false;
                    continue;
                }
            }
            if (success) {
                w.push_back(t);
                i += 6;
            }
        } else {
            w.push_back(static_cast<wchar_t>(s[i]));
            ++i;
        }
    }
    return w;
}

std::string to_ascii(const std::wstring& s)
{
    return boost::locale::conv::utf_to_utf<char>(s);
}

}
