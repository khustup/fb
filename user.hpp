#pragma once

#include "date.hpp"
#include "gender.hpp"

#include <string>
#include <vector>

namespace fb {

/**
 * @class fb::user.
 * @brief Structure to store user related info.
 */
struct user
{
    /// @brief Default constructor.
    user()
        : id()
        , first_name()
        , last_name()
        , gender(base::UNKNOWN)
        , birth_date()
        , location()
        , email()
        , friends()
        , liked_pages()
    {}

    /// @brief User ID.
    std::string id;

    /// @brief First name.
    std::wstring first_name;

    /// @brief Last name.
    std::wstring last_name;

    /// @brief Gender.
    base::gender gender;

    /// @brief Birth date.
    base::date birth_date;

    /// @brief Location.
    std::wstring location;

    /// @brief Email.
    std::string email;

    /// @brief Friends list.
    std::vector<std::string> friends;

    /// @brief Liked pages list.
    std::vector<std::string> liked_pages;
};

}
