#include "date.hpp"

namespace base {

void date::check_date(unsigned y, unsigned m, unsigned d)
{
    if (m == 2 && d == 29 && y % 4 != 0) {
        throw invalid_date("Invalid date provided");
    }
    if (m == 0 || d == 0 || m > 12 || d > 31) {
        throw invalid_date("Invalid date provided");
    }
    if (m == 2 && d == 30) {
        throw invalid_date("Invalid date provided");
    }
    if (m == 4 || m == 6 || m == 9 || m == 11) {
        if (d == 31) {
            throw invalid_date("Invalid date provided");
        }
    }
}

}
